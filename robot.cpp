#include "robot.h"

bool Robot::connect()
{
    if(!isConnected() && connectToVisionDevice() && connectToMotorControlDevice())
    {
        port.set_option(boost::asio::serial_port_base::baud_rate(9600));
        port.set_option(boost::asio::serial_port_base::parity());
        port.set_option(boost::asio::serial_port_base::stop_bits());
        port.set_option(boost::asio::serial_port_base::flow_control());
        port.set_option(boost::asio::serial_port_base::character_size(8));
        //connected=true;
        return true;
    }
    return false;
}

double Robot::getFrameWidth()
{
    if(isConnected())
    {
        return cap.get(CV_CAP_PROP_FRAME_WIDTH);
    }
    std::cerr<<"Error: Called getFrameWidth() on unconnected Robot object\n";
    return 0.0;
}

double Robot::getFrameHeight()
{
    if(isConnected())
    {
        return cap.get(CV_CAP_PROP_FRAME_HEIGHT);
    }
    std::cerr<<"Error: Called getFrameHeight() on unconnected Robot object\n";
    return 0.0;
}

double Robot::getFrameRate()
{
    if(isConnected())
    {
        return cap.get(CV_CAP_PROP_FPS);
    }
    std::cerr<<"Error: Called getFrameRate() on unconnected Robot object\n";
    return 0.0;
}

void Robot::pick()
{
    lowerArm();
    act(DO_NOTHING, 20, false);
    grab();
    act(DO_NOTHING, 20, false);
    upArm();
    act(MOVE_BACKWARD, 1000);
}
void Robot::lowerArm()
{
    unsigned milliseconds=1150;
    boost::asio::deadline_timer t(is, boost::posix_time::milliseconds(milliseconds));
    char c='h';
    boost::asio::write(port, boost::asio::buffer(&c,1));
    t.wait();
    stop();;
}


void Robot::upArm()
{
    unsigned milliseconds=1100;
    boost::asio::deadline_timer t(is, boost::posix_time::milliseconds(milliseconds));
    char c='y';
    boost::asio::write(port, boost::asio::buffer(&c,1));
    t.wait();
    stop();
}

void Robot::grab()
{
    unsigned milliseconds=1500;
    boost::asio::deadline_timer t(is, boost::posix_time::milliseconds(milliseconds));
    char c='l';
    boost::asio::write(port, boost::asio::buffer(&c,1));
    t.wait();
    stop();;
}

void Robot::open()
{
    unsigned milliseconds=1500;
    boost::asio::deadline_timer t(is, boost::posix_time::milliseconds(milliseconds));
    char c='o';
    boost::asio::write(port, boost::asio::buffer(&c,1));
    t.wait();
    stop();;
}




bool Robot::getNextFrame(CV_OUT cv::Mat &image)
{
    if(isConnected())
    {
        return cap.read(image);
    }
    std::cerr<<"Error: Called getNextFrame() on unconnected Robot object\n";
    return false;
}

void Robot::moveForward()
{
    if(isConnected() && !isMovingForward())
    {
        char c='w';
        boost::asio::write(port,boost::asio::buffer(&c,1), e);
        if(!e)
        {
            #ifdef LOG
            std::cout<<"Moving forward immediately\n";
            #endif // LOG
            movingForward=true;
        }
    }
}

void Robot::moveBackward()
{
    if(isConnected() && !isMovingBackward())
    {

        char c='s';
        boost::asio::write(port,boost::asio::buffer(&c,1), e);
        if(!e)
        {
            #ifdef LOG
            std::cout<<"Moving backward immediately\n";
            #endif // LOG
            movingBackward=true;
        }
    }
}

void Robot::stop()
{
    if(isConnected() && !isStationary())
    {
        char c='q';
        boost::asio::write(port,boost::asio::buffer(&c,1), e);
        if(!e)
        {
            #ifdef LOG
            std::cout<<"Stopping immediately\n";
            #endif // LOG
            movingBackward=movingForward=rotatingToLeft=rotatingToRight=false;
        }
    }
}

void Robot::async_delayedStop(unsigned long milliseconds)
{
    auto t=std::make_shared<boost::asio::deadline_timer>(is, boost::posix_time::milliseconds(milliseconds));
    t->async_wait([this,t](boost::system::error_code e)
    {
        stop();
    });
}

void Robot::async_rotateLeftFor(unsigned long milliseconds)
{
    if(isInMotion())
        async_delayedStop(50);
    auto t=std::make_shared<boost::asio::deadline_timer>(is, boost::posix_time::milliseconds(milliseconds));
    char c='d';
    boost::asio::write(port, boost::asio::buffer(&c,1));
    rotatingToLeft=true;
    t->async_wait([this,t](boost::system::error_code e)
    {
        stop();
    } );
}

void Robot::async_rotateRightFor(unsigned long milliseconds)
{
    auto t=std::make_shared<boost::asio::deadline_timer>(is, boost::posix_time::milliseconds(milliseconds));
    char c='a';
    boost::asio::write(port, boost::asio::buffer(&c,1));
    rotatingToRight=true;
    t->async_wait([this,t](boost::system::error_code e)
    {
        stop();
    } );
}


void Robot::rotateLeft()
{
    if(isConnected() && !isRotatingToLeft())
    {
        char c='d';
        boost::asio::write(port,boost::asio::buffer(&c,1),e);
        if(!e)
        {

            #ifdef LOG
            std::cout<<"Rotating to left immediately\n";
            #endif // LOG
            rotatingToLeft=true;
        }
    }
}

void Robot::rotateRight()
{
    if(isConnected() && !isRotatingToRight())
    {
        char c='a';
        boost::asio::write(port,boost::asio::buffer(&c,1),e);
        if(!e)
        {
            #ifdef LOG
            std::cout<<"Rotating to right immediately\n";
            #endif // LOG
            rotatingToRight=true;
        }
    }
}

void Robot::act(Action a, unsigned long milliseconds, bool doStop)
{
    if(a==MOVE_FORWARD)
        moveForward();
    else if(a==MOVE_BACKWARD)
        moveBackward();
    else if(a==ROTATE_LEFT)
        rotateLeft();
    else if(a==ROTATE_RIGHT)
        rotateRight();
    else if(a==STOP)
        stop();
    else if(a==DO_NOTHING);
    boost::asio::deadline_timer t(is, boost::posix_time::milliseconds(milliseconds));
    #ifdef LOG
    std::cout<<"Waiting for "<<milliseconds<<" ms synchronously\n";
    #endif // LOG
    t.wait();
    if(doStop)
        stop();
}


bool Robot::connectToVisionDevice()
{
    std::cout<<"Connection to robot's vision component initiated\n";
    try
    {
        int deviceId=std::stoi(visionDevice);
        std::cout<<"Trying to connect to camera number "<<deviceId<<"..."<<std::endl;
        cap.open(deviceId); // open the camera using integral device number 0,...
    }
    catch(std::out_of_range err)
    {
        std::cerr<<"Error: Device address for camera initialization is too large\n";
        return false;
    }
    catch(std::invalid_argument err)
    {
        std::cout<<"Trying to open camera at "<<visionDevice<<"..."<<std::endl;
        cap.open(visionDevice);
    }

    if(!cap.isOpened())  // check if we succeeded
    {
        std::cerr<<"Error: Could not connect to camera\n";
        return false;
    }
    //Camera opened successfully
    std::cout<<"Connection to robot's vision component established successfully"<<std::endl;
    //cap.set(CV_CAP_PROP_FPS, 10);
    return true;
}

bool Robot::connectToMotorControlDevice()
{
    std::cout<<"Connection to robot's motor control component initiated\n";
    try
    {
        port.open(motorControlDevice);
    }
    catch(boost::system::system_error e)
    {
        std::cerr<<"Error: Connection to robot's motor control component failed\n";
        std::cerr<<"Details: "<<e.what()<<"\n";;
        return false;
    }
    std::cout<<"Connection to robot's motor control component established successfully\n";
    return true;
}


