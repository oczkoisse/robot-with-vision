#include <cstdlib>
#include <iostream>
#include <string>

#include <opencv2/opencv.hpp>

#include "robot.h"

#define DEBUG

void showHelp();
void loadCalibrationParam(const std::string &a, cv::Mat_<double> &dc, cv::Mat_<double> &cm);
void showSignalProperties(Robot &);

enum Location {UNKNOWN, LEFT,CENTER,RIGHT};
int main(int argc, char** argv )
{
#ifndef DEBUG
    cv::redirectError(cvNulDevReport);
#endif
    //These are all arguments to the program
    std::string address, portAddress;
    bool nightmode=false, showExtraWindows=false, cameraInvert=false, calibration=false;

    //The user must supply address as the first argument
    //To turn night mode on, supply -n switch
    //To show all windows, probably for fine tuning, provide -a switch
    //If the original camera is inverted, then you should supply -i switch for upright image in windows

    if(argc<=2)
    {
        std::cerr<<"Error: Insufficient arguments\n";
        showHelp();
        return 0;
    }
    else if(argc>=3 && argc<=7)
    {
        address=argv[1];
        portAddress=argv[2];
        for(int i=3; argv[i]!=0; ++i)
        {
            std::string a(argv[i]);
            if(a=="-i")
                cameraInvert=true;
            else if(a=="-w")
                showExtraWindows=true;
            else if(a=="-n")
                nightmode=true;
            else if(a=="-c")
                calibration=true;
            else
            {
                std::cerr<<"Error: Unrecognized command line switch\n";
                showHelp();
                return 0;
            }
        }
    }
    else
    {
        std::cerr<<"Error: Excess arguments\n";
        showHelp();
        return 0;
    }

    cv::Mat_<double> cameraMatrix, distortionCoefficients;
    if(calibration)
    {
        loadCalibrationParam("calibData/calibData.xml", distortionCoefficients, cameraMatrix);
    }

    Robot dexter(address, portAddress);
    if(!dexter.connect())
        std::exit(0);
    showSignalProperties(dexter);

    cv::namedWindow("Capture Window");
    if(showExtraWindows)
        cv::namedWindow("Processed Image");
    if(nightmode)
        cv::namedWindow("After contrast increase");

    //Default values are for blueish hue
    int hMin=0,sMin=60,vMin=0, hMax=25, sMax=255, vMax=255;
    cv::namedWindow("Color Range",1);
    cv::createTrackbar("Hue Min","Color Range",&hMin,180);
    cv::createTrackbar("Hue Max","Color Range",&hMax,180);
    cv::createTrackbar("Sat Min","Color Range",&sMin,255);
    cv::createTrackbar("Sat Max","Color Range",&sMax,255);
    cv::createTrackbar("Value Min","Color Range",&vMin,255);
    cv::createTrackbar("Value Max","Color Range",&vMax,255);

    //The following are the detection algorithm parameters
    int frameHeight=dexter.getFrameHeight(), frameWidth=dexter.getFrameWidth();
    int limitRad=frameHeight<frameWidth?(frameHeight/2):(frameWidth/2);
    int minRad=0.25*limitRad, maxRad=0.75*limitRad, circCover=50, stopRadius=50;
    cv::namedWindow("Parameters",CV_WINDOW_AUTOSIZE);
    cv::createTrackbar("Radius Min", "Parameters", &minRad, limitRad);
    cv::createTrackbar("Radius Max", "Parameters", &maxRad, limitRad);
    cv::createTrackbar("%Circle Min", "Parameters", &circCover, 100);
    cv::createTrackbar("Stopping radius", "Parameters", &stopRadius, 100);
    Location l= UNKNOWN;
    bool follow=false;
    //Loop runs as long as Esc key is not pressed
    while(true)//Esc code is 27
    {
        int k=cv::waitKey(20);
        if(k==27)
            break;
        else if(k=='t')
            follow=!follow;
        if(!follow)
            l=UNKNOWN;
        cv::Mat frame;
        // get a new frame from camera
        if(!dexter.getNextFrame(frame))
        {
            std::cerr<<"Error: Connection lost.\nExiting...\n";
            cv::destroyAllWindows();
            return 0;
        }

        cv::Mat undistortedFrame;
        if(calibration)
            cv::undistort(frame,undistortedFrame,cameraMatrix,distortionCoefficients);
        else
            frame.copyTo(undistortedFrame);

        if(cameraInvert)
            cv::flip(undistortedFrame, undistortedFrame, -1);


        //Smoothing the image to remove noise
        cv::medianBlur(undistortedFrame,undistortedFrame,3);

        cv::Mat hsvFrame;
        cv::cvtColor(undistortedFrame,hsvFrame,cv::COLOR_BGR2HSV);

        if(nightmode)
        {
            //Histogram Equalization follows
            std::vector<cv::Mat> hsvChannels;
            //Split hsv frame to individual channels
            cv::split(hsvFrame,hsvChannels);
            //Apply histogram equalization on value/intensity channel
            cv::Mat equalizedVal;
            cv::equalizeHist(hsvChannels[2],equalizedVal);
            hsvChannels[2]=equalizedVal;
            //Merge the modified hsv channels into a single image and copy to hsvFrame
            cv::merge(hsvChannels,hsvFrame);

            //Show the image after increased contrast
            cv::Mat rgbFrame;
            cv::cvtColor(hsvFrame, rgbFrame,cv::COLOR_HSV2BGR);
            cv::imshow("After contrast increase", rgbFrame);
        }

        //Thresholding in HSV space
        cv::Mat thresFrame;
        if(hMin<=hMax)
            cv::inRange(hsvFrame, cv::Vec<int,3>(hMin,sMin,vMin), cv::Vec<int,3>(hMax,sMax,vMax),thresFrame);
        else
        {
            cv::Mat t1, t2;
            cv::inRange(hsvFrame, cv::Vec<int,3>(hMin,sMin,vMin), cv::Vec<int,3>(180,sMax,vMax),t1);
            cv::inRange(hsvFrame, cv::Vec<int,3>(0,sMin,vMin), cv::Vec<int,3>(hMax,sMax,vMax),t2);
            thresFrame=t1|t2;
        }
        hsvFrame.release();

        //Find the extreme outer counters only, storing all the contour points
        std::vector<std::vector<cv::Point>> contours;
        cv::findContours(thresFrame,contours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_NONE);

        thresFrame.release();

        //The contour frame has a white background and is a single channel 8 bit (0-255) image
        cv::Mat contourFrame(frame.size(), CV_8U, cv::Scalar(255));
        //Draw all the found contours with black color with thickness 1
        cv::drawContours(contourFrame, contours, -1, cv::Scalar(0), 1);

        std::vector<cv::Point2f> centres(contours.size());
        std::vector<float> radii(contours.size());

        int maxCircle=-1;
        double maxCirclePercentage=-1.0;
        for(int i=0; i<contours.size(); i++)
        {
            //Find minimum enclosing circle fir all the found contours
            cv::minEnclosingCircle(contours[i],centres[i], radii[i]);

            double circArea=3.14*radii[i]*radii[i], contArea=cv::contourArea(contours[i]);
            double circPercentage=(contArea*100/circArea);

            //Only draw enclosing circle if radius is in acceptable range
            //and contour area is at least areaCoverage % of the enclosing circle

            if((radii[i]>=minRad && radii[i]<=maxRad) && circPercentage >= circCover)
            {
                if(circPercentage > maxCirclePercentage)
                {
                    maxCirclePercentage=circPercentage;
                    maxCircle=i;
                }
            }
        }

        dexter.completePendingMotions();
        if(maxCircle!=-1)
        {
            cv::circle(undistortedFrame, centres[maxCircle], radii[maxCircle], cv::Scalar(255,0,0), 2);



            if(follow)
            {
                /*
                if(radii[maxCircle] >= stopRadius)
                {
                    dexter.async_delayedStop(50);
                    dexter.pick();
                    dexter.stop();
                    follow=false;
                    while(cv::waitKey(10000)!=27);
                    return 0;
                }
*/
                int distanceFromCenter=std::abs(centres[maxCircle].x - frameWidth/2);
                //target on center
                if(distanceFromCenter<=50)
                {
                    l=CENTER;
                    if(dexter.isRotatingToLeft()||dexter.isRotatingToRight()||dexter.isMovingBackward())
                    {
                        dexter.async_delayedStop(50);
                    }
                    //move forward
                    else if(!dexter.isMovingForward())
                        dexter.moveForward();
                }
                //target off center
                else
                {
                    //need to align
                    if(centres[maxCircle].x<(frameWidth/2))
                    {
                        l=LEFT;
                        if(!dexter.isRotatingToLeft())
                        {
                            dexter.async_rotateLeftFor(50);
                        }
                        else
                            dexter.async_delayedStop(50);
                    }
                    else//need to align right
                    {
                        l=RIGHT;
                        if(!dexter.isRotatingToRight())
                        {
                            dexter.async_rotateRightFor(50);
                        }
                        else
                            dexter.async_delayedStop(50);
                    }
                }
            }
        }
        else
        {
            if(follow)
            {
                if(dexter.isMovingForward()||dexter.isMovingBackward() ||dexter.isRotatingToRight())
                {
                    dexter.async_delayedStop(50);
                }
                else if(l==LEFT && !dexter.isRotatingToLeft())
                    dexter.async_rotateLeftFor(50);
                else if(l==RIGHT && !dexter.isRotatingToRight())
                    dexter.async_rotateRightFor(50);
                else
                    dexter.async_rotateLeftFor(50);
                l=UNKNOWN;
            }
        }
        if(showExtraWindows)
            cv::imshow("Processed Image", contourFrame);
        cv::imshow("Capture Window", undistortedFrame);
    }
    return 0;
}

void showHelp()
{
    std::cerr<<"Usage: av [source] <options>\n";
    std::cerr<<"Options:\n";
    std::cerr<<"-n Night Mode On, implicitly turns on the \"After Contrast Increase\" Window (By default Off)\n";
    std::cerr<<"-a Show All Windows (By default Off)\n";
    std::cerr<<"-i Invert the Camera Input (By default Off)\n";
}

void loadCalibrationParam(const std::string &xmlFile, cv::Mat_<double> &distortionCoefficients, cv::Mat_<double> &cameraMatrix)
{
    std::cout<<"Loading camera parameters...\n";
    cv::FileStorage fs(xmlFile,cv::FileStorage::READ);
    if(fs.isOpened())
    {
        std::cout<<"XML File opened successfully...\n";
        std::cout<<"Reading camera matrix...\n";
        cv::FileNode cm=fs["Camera_Matrix"];
        cm>>cameraMatrix;
        std::cout<<"Reading distortion coefficients...\n";
        cv::FileNode dc=fs["Distortion_Coefficients"];
        dc>>distortionCoefficients;
    }
    else
    {
        std::cerr<<"Error: Unable to open XML file. Exiting..."<<std::endl;;
        std::exit(0);
    }
}



void showSignalProperties(Robot &r)
{
    std::cout<<"Signal Properties:\n";
    std::cout<<"Resolution: "<<r.getFrameWidth()<<"x"<<r.getFrameHeight()<<"\n";
    std::cout<<"Frame Rate: "<<r.getFrameRate()<<std::endl;
}
