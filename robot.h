#ifndef ROBOT_H_INCLUDED
#define ROBOT_H_INCLUDED

#include <opencv2/opencv.hpp>
#include <boost/asio.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>

//#define LOG

class Robot
{
    //bool connected=false;
    /*The following are related to the vision sensor*/
    //Connection to vision sensor
    cv::VideoCapture cap;
    //deviceId is the http address or the device number of the vision sensor of the robot
    std::string visionDevice;

    /*The following are related to the motor control sensor*/
    //io service to interact with underlying OS
    boost::asio::io_service is;
    //Connection for motor control
    boost::asio::serial_port port;
    boost::system::error_code e;
    //The name of the com port
    std::string motorControlDevice;

    //bool follow=false;

    /*The following are the motion state checks for the robot*/
    //It is the duty of the class to ensure that all of these are always mutually exclusive
    bool movingForward=false, movingBackward=false, rotatingToLeft=false, rotatingToRight=false;

public:
    enum Action {MOVE_FORWARD, MOVE_BACKWARD, ROTATE_LEFT, ROTATE_RIGHT, STOP, DO_NOTHING};
    Robot(const std::string &visionDeviceAddress, const std::string &motorControlDeviceAddress)
        :visionDevice(visionDeviceAddress), port(is), motorControlDevice(motorControlDeviceAddress)
    {}

    ~Robot()
    {
        port.close();
    }

    bool connect();

    void disconnect()
    {
        //Will implement later
        //connected=false;
    }

    bool isConnected() const
    {
        return cap.isOpened() && port.is_open();
    }

    void lowerArm();
    void upArm();
    void grab();
    void open();
    void pick();
    /*void setFollowStatus(bool x)
    {
        follow=x;
    }

    bool getFollowStatus()
    {
        return follow;
    }
    */
    double getFrameWidth();

    double getFrameHeight();

    double getFrameRate();

    bool getNextFrame(CV_OUT cv::Mat &image);

    void async_rotateLeftFor(unsigned long milliseconds);

    void async_rotateRightFor(unsigned long milliseconds);

    void moveForward();

    void moveBackward();

    void rotateLeft();

    void rotateRight();

    void stop();
    void async_delayedStop(unsigned long milliseconds);

    void act( Action a, unsigned long milliseconds=0, bool doStop=true);


    bool isInMotion() const
    {
        return movingForward||movingBackward||rotatingToLeft||rotatingToRight;
    }

    bool isStationary() const
    {
        return !isInMotion();
    }

    bool isMovingForward() const
    {
        return movingForward && !movingBackward && !rotatingToLeft && !rotatingToRight;
    }

    bool isMovingBackward() const
    {
        return !movingForward && movingBackward && !rotatingToLeft && !rotatingToRight;

    }

    bool isRotatingToLeft() const
    {
        return !movingForward && !movingBackward && rotatingToLeft && !rotatingToRight;
    }

    bool isRotatingToRight() const
    {
        return !movingForward && !movingBackward && !rotatingToLeft && rotatingToRight;
    }

    void completePendingMotions()
    {
        is.run();
    }

private:
    bool connectToVisionDevice();

    bool connectToMotorControlDevice();

};


#endif // ROBOT_H_INCLUDED
